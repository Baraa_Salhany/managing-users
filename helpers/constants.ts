export default class Constants {
  /* constants: Strings */
  static EMAIL_FROM = 'ManagingUsers <baraa.salhany@smartretail.co>';
  static EMAIL_REGEX =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  /* constants: Strings */

  /* constants: Objects */
  static ROLES = {
    ADMIN: 'ADMIN',
    USER: 'USER',
  } as const;

  static TYPES = {
    STRING: 'string',
  };
  /* constants: Objects */

  /* constants: Arrays */
  static ROLES_LIST = Object.values(Constants.ROLES);
  static EXCLUDED_COLUMNS = ['createdAt', 'updatedAt', 'deletedAt'];
  static USER_EXCLUDED_COLUMNS = [
    ...Constants.EXCLUDED_COLUMNS,
    'password',
    'email_token',
  ];
  static REQUIRED_ENVIRONMENT_VARIABLES = [
    'PORT',
    'SERVER_URL',
    'PSQL_HOST',
    'PSQL_USER',
    'PSQL_PASSWORD',
    'PSQL_DB',
    'JWT_SECRET',
    'EMAIL_VERIFICATION_TOKEN_VALIDITY',
    'SESSION_ACTIVE_TIME',
    'SENDGRID_API_KEY',
  ];
  /* constants: Arrays */
}
