import { Request, Response, NextFunction } from 'express';
import { User } from '../models';

declare module 'express-serve-static-core' {
  interface Request {
    userDetails?: User;
    emailToken?: string;
  }
}

export { Request, Response, NextFunction };

export interface UpdatableUserFields {
  first_name: string;
  last_name: string;
  state: string;
  city: string;
  address1: string;
  address2: string;
  country_id: number;
}
