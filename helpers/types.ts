import Constants from './constants';

export type Role = keyof typeof Constants.ROLES;
