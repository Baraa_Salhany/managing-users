import { Request, Response, Router } from 'express';

import authRouter from './auth.router';
import userRouter from './user.router';

const router = Router();

// status endpoint
router.get('/status', (req: Request, res: Response) => {
  res.send({ message: 'Managing Users API is Working!' });
});

router.use('/auth', authRouter);
router.use('/users', userRouter);

export default router;
