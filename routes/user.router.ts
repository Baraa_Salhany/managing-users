import { Router } from 'express';

import * as authController from '../controllers/auth.controller';
import * as userController from '../controllers/user.controller';

const router = Router();

// Get user details
router.get('/', authController.verifyToken, userController.getUser);

// Get all users (Just user with 'ADMIN' role can access this endpoint)
router.get(
  '/list',
  authController.verifyToken,
  authController.isAdmin, // Check that user has 'ADMIN' role
  userController.getUsers
);

// Update user details
router.put('/', authController.verifyToken, userController.updateUser);

export default router;
