import {
  DataTypes,
  Model,
  InferAttributes,
  InferCreationAttributes,
  CreationOptional,
  NonAttribute,
  ForeignKey,
} from 'sequelize';

import Constants from '../helpers/constants';
import { Role } from '../helpers/types';

import { sequelize } from './connection';
import { Country } from './country.model';

// User class
export class User extends Model<
  InferAttributes<User>,
  InferCreationAttributes<User>
> {
  declare id: CreationOptional<number>;
  declare country_id: ForeignKey<Country['id']>;
  declare email: string;
  declare username: string;
  declare first_name: string;
  declare last_name: string;
  declare email_verified: CreationOptional<boolean>;
  declare email_token: string;
  declare password: string;
  declare role: CreationOptional<Role>;
  declare state: string;
  declare city: string;
  declare address1: string;
  declare address2: CreationOptional<string>;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;
  declare deletedAt: CreationOptional<Date>;

  declare country?: NonAttribute<Country>;
}

// Define attributes of User model
User.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    country_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING(40),
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true,
      },
    },
    username: {
      type: DataTypes.STRING(30),
      allowNull: false,
      unique: true,
      validate: {
        len: {
          args: [5, 30],
          msg: 'Username length should be between 5 and 30 characters',
        },
      },
    },
    first_name: {
      type: DataTypes.STRING(20),
      allowNull: false,
      validate: {
        len: {
          args: [5, 20],
          msg: 'First name length should be between 5 and 20 characters',
        },
      },
    },
    last_name: {
      type: DataTypes.STRING(20),
      allowNull: false,
      validate: {
        len: {
          args: [5, 20],
          msg: 'Last name length should be between 5 and 20 characters',
        },
      },
    },
    email_verified: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    email_token: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    role: {
      type: DataTypes.ENUM,
      values: Constants.ROLES_LIST,
      defaultValue: Constants.ROLES.USER,
      allowNull: false,
    },
    state: {
      type: DataTypes.STRING(30),
      allowNull: false,
    },
    city: {
      type: DataTypes.STRING(30),
      allowNull: false,
    },
    address1: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    address2: {
      type: DataTypes.STRING(100),
    },
    createdAt: {
      type: DataTypes.DATE,
    },
    updatedAt: {
      type: DataTypes.DATE,
    },
    deletedAt: {
      type: DataTypes.DATE,
    },
  },
  {
    tableName: 'user',
    sequelize,
  }
);
