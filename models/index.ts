import { sequelize } from './connection';
import { User } from './user.model';
import { Country } from './country.model';

/* Create associations between models */
Country.hasMany(User, { foreignKey: 'country_id' });
User.belongsTo(Country, { foreignKey: 'country_id', as: 'country' });
/* Create associations between models */

export { sequelize, User, Country };
