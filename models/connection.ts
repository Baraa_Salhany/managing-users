import { Sequelize } from 'sequelize';

const PSQL_HOST = process.env.PSQL_HOST as string;
const PSQL_USER = process.env.PSQL_USER as string;
const PSQL_PASSWORD = process.env.PSQL_PASSWORD as string;
const PSQL_DB = process.env.PSQL_DB as string;

//creating sequelizer ORM object using the database config from .env file
const sequelize = new Sequelize(PSQL_DB, PSQL_USER, PSQL_PASSWORD, {
  host: PSQL_HOST,
  dialect: 'postgres',
  logging: false,

  define: {
    timestamps: true,
    paranoid: true, // to enable soft deletion
  },
});

export { sequelize };
