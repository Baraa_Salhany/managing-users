- This repo have the code of Managing Users API.
- Node version: v14.6.0
- To test the API, use the swagger documentation on this link: https://managing-users.herokuapp.com/api-docs

Notes:
Since users-list endpoint accessible just for users with "ADMIN" role, you can use the following credentials to test:
username: "Baraa1",
password: "Test1234"
