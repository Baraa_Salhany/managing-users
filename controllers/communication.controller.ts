import * as sgMail from '@sendgrid/mail';
import * as Mustache from 'mustache';

import Constants from '../helpers/constants';
import { Request, Response } from '../helpers/interfaces';
import { logErrorAndFillResponse } from './helper.controller';

//email templates
import { email_verification_template } from '../email_templates/email_verification';

// Set api-key to send emails with Sendgrid
sgMail.setApiKey(process.env.SENDGRID_API_KEY || '');

/**
 * Send verification email to new registered user
 * @param req  Express Request
 * @param res Express Response
 */
export async function sendVerificationEmail(req: Request, res: Response) {
  try {
    // Payload for email template
    const payload = {
      server_url: process.env.SERVER_URL,
      username: req.body.user.username,
      email_token: req.emailToken,
    };

    const msg = {
      to: req.body.user.email,
      from: Constants.EMAIL_FROM,
      subject: 'Please Verify Your Email',
      html: Mustache.render(email_verification_template, payload),
    };

    await sgMail.send(msg);

    res.send({
      message:
        'You have been registered successfully, please verify your email.',
    });
  } catch (error) {
    logErrorAndFillResponse(res, 'sendVerificationEmail', error);
  }
}
