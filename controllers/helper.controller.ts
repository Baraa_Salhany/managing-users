import Constants from '../helpers/constants';
import { Response } from '../helpers/interfaces';

/**
 * Check if any of required environment variables is missing
 */
export function checkMissingEnvironmentVariables(): void {
  for (const envVar of Constants.REQUIRED_ENVIRONMENT_VARIABLES) {
    if (process.env[envVar] == undefined) {
      console.error(`~~~ Enviroment variable is missing: '${envVar}'`);
    }
  }
}

/**
 * Log the error message and return error to user
 * @param res Express Response
 * @param functionName Name of the function where the error occurred
 * @param error The catched error object
 * @param message Custom message to be returned to user in the response
 */
export function logErrorAndFillResponse(
  res: Response,
  functionName: string,
  error: any,
  error_message = 'Internal error occurred.'
): void {
  console.error(`### Error inside '${functionName}' function: ${error}`);
  res.status(500).send({ error_message: error.message || error_message });
}

/**
 * Capitalize first letter of a word
 * @param word The original word
 * @returns The word after capitalizing first letter of it
 */
function capitalizeFirstLetter(word: string): string {
  return word.charAt(0).toUpperCase() + word.slice(1);
}

/**
 * Get a proper name of attribute to be displayed to user
 * by splitting the attribute name to words and capitalize first letter of each word
 * @param attribute The name of attribute
 * @returns A proper name to be displayed to user
 */
export function getDisplayNameOfAttribute(attribute: string): string {
  const wordsList = attribute.split('_');
  const capitalizedWordsList = wordsList.map(capitalizeFirstLetter);
  const displayName = capitalizedWordsList.join(' ');
  return displayName;
}

/**
 * Chaeck if an object contains a list of required fields, and if not will return an array of
 * missing fields
 * @param requiredFields Array of required fields names
 * @param mainObject The object to be checked
 * @param missing Array of missing fields (if any)
 */
export function checkRequiredFieldsInObject(
  requiredFields: string[],
  mainObject: any,
  missing: string[]
): void {
  for (const field of requiredFields) {
    if (!mainObject[field]) {
      missing.push(getDisplayNameOfAttribute(field));
    }
  }
}

/**
 * Check that list of object attributes have a specific type, and if not will return an array of
 * attributes which mismatch the expected type
 * @param fields Array of attributes names
 * @param mainObject The object to be checked
 * @param expectedType The expected types of attributes
 * @param mismatch Array of attributes which mismatch the expected type
 */
export function checkTypeOfFields(
  fields: string[],
  mainObject: any,
  expectedType: string,
  mismatch: string[]
): void {
  for (const field of fields) {
    if (typeof mainObject[field] != expectedType) {
      mismatch.push(getDisplayNameOfAttribute(field));
    }
  }
}

/**
 * Trim the values of list of string attributes in object
 * @param fields Array of attributes names
 * @param mainObject The object
 */
export function trimValuesOfFieldsInObject(fields: string[], mainObject: any) {
  for (const field of fields) {
    mainObject[field] = mainObject[field].trim();
  }
}

/**
 * Trim the values of list of string attributes in object if they are exist,
 * and return array of attributes which mismatch 'string' type
 * @param fields
 * @param mainObject
 * @param mismatch
 */
export function checkAndTrimStringFieldsIfExist(
  fields: string[],
  mainObject: any,
  mismatch: string[]
): void {
  for (const field of fields) {
    if (mainObject[field] != undefined) {
      if (typeof mainObject[field] != Constants.TYPES.STRING) {
        mismatch.push(getDisplayNameOfAttribute(field));
      } else {
        mainObject[field] = mainObject[field].trim();
        if (!mainObject[field]) {
          delete mainObject[field];
        }
      }
    }
  }
}
