import Constants from '../helpers/constants';
import { Request, Response, UpdatableUserFields } from '../helpers/interfaces';
import { User, Country } from '../models';
import {
  logErrorAndFillResponse,
  checkAndTrimStringFieldsIfExist,
} from './helper.controller';

/**
 * Get user details
 * @param req  Express Request
 * @param res Express Response
 * @returns User details object
 */
export async function getUser(req: Request, res: Response) {
  try {
    return res.send({
      user: req.userDetails,
    });
  } catch (error) {
    logErrorAndFillResponse(res, 'getUser', error);
  }
}

/**
 * Get list of all users with pagination
 * @param req  Express Request
 * @param res Express Response
 * @returns Object contains the total number of users, and list of users-details based on the selected page
 */
export async function getUsers(req: Request, res: Response) {
  try {
    let pageSizeAsString = req.query.page_size as string;
    let pageNumberAsString = req.query.page_number as string;
    const pageSize = parseInt(pageSizeAsString);
    const pageNumber = parseInt(pageNumberAsString);
    if (!Number.isInteger(pageSize)) {
      return res.status(400).send({ error_message: 'Invalid page size' });
    }
    if (!Number.isInteger(pageNumber)) {
      return res.status(400).send({ error_message: 'Invalid page number' });
    }

    // Limit the page size up to 500
    if (pageSize > 500) {
      return res
        .status(400)
        .send({
          error_message: `Invalid page size. Page size shouldn't exceed 500`,
        });
    }
    if (pageSize < 5) {
      return res.status(400).send({
        error_message: `Invalid page size. Page size should be greater than 4`,
      });
    }

    if (pageNumber < 1) {
      return res.status(400).send({
        error_message: `Invalid page number. Page number should be greater than 0`,
      });
    }

    const users = await User.findAndCountAll({
      order: [['id', 'ASC']],
      limit: pageSize,
      offset: (pageNumber - 1) * pageSize,
      attributes: {
        exclude: Constants.USER_EXCLUDED_COLUMNS,
      },
      include: [
        {
          model: Country,
          as: 'country',
          attributes: ['name', 'code'],
        },
      ],
    });

    return res.send({ users });
  } catch (error) {
    logErrorAndFillResponse(res, 'getUsers', error);
  }
}

/**
 * Update user details
 * @param req  Express Request
 * @param res Express Response
 * @returns Success message
 */
export async function updateUser(req: Request, res: Response) {
  try {
    if (!req.body.user) {
      return res.status(400).send({
        error_message: 'user informations are missing!',
      });
    }

    const updatableStringFields = [
      'first_name',
      'last_name',
      'state',
      'city',
      'address1',
      'address2',
    ];

    const invalidStringList: string[] = [];
    checkAndTrimStringFieldsIfExist(
      updatableStringFields,
      req.body.user,
      invalidStringList
    );

    if (invalidStringList.length == 1) {
      return res.status(400).send({
        error_message: `Invalid ${invalidStringList[0]}!`,
      });
    }

    if (invalidStringList.length > 1) {
      return res.status(400).send({
        error_message: `The following fields have invalid values: ${invalidStringList}`,
      });
    }

    if (req.body.user.country_id) {
      if (!Number.isInteger(req.body.user.country_id)) {
        return res.status(400).send({
          error_message: `Invalid country!`,
        });
      }

      const country = await Country.findByPk(req.body.user.country_id);

      if (!country) {
        return res.status(400).send({
          error_message: `Invalid country!`,
        });
      }
    }

    let updatedFields: UpdatableUserFields = {
      first_name: req.body.user.first_name || req.userDetails?.first_name,
      last_name: req.body.user.last_name || req.userDetails?.last_name,
      state: req.body.user.state || req.userDetails?.state,
      city: req.body.user.city || req.userDetails?.city,
      address1: req.body.user.address1 || req.userDetails?.address1,
      address2: req.body.user.address2 || req.userDetails?.address2,
      country_id: req.body.user.country_id || req.userDetails?.country_id,
    };

    await User.update(updatedFields, {
      where: {
        id: req.userDetails?.id,
      },
    });

    return res.send({
      message: 'Your information has been updated successfully!',
    });
  } catch (error) {
    logErrorAndFillResponse(res, 'updateUser', error);
  }
}
